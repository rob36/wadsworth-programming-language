#Wadsworth 
####An introduction to the butler-inspired programming language 

- - -
**What is Wadsworth?**
    Wadsworth is a programming language created using ANTLR and Java. Compiling to Java ByteCode using Jasmin and written to be syntactically much like English, it aims to be both easy to read and understand, while offering the same features as any other basic expression language, with a couple of added extras. Of course, as any good butler would, Wadsworth is always eager to learn more, and can always be expanded to allow for more functionalities.

**So, what can Wadsworth do for me?**
    While Wadsworth sadly cannot clean your house, pour your guests a great 12 year old Scotch or cook you dinner, he will have no difficulties performing basic arithmetics like additions, multiplications and remainder calculations, boolean algebra and if- and while loops (of course, you would not want to give poor old Wadsworth the same instructions over and over again!). He can even ask for user input if needed, or tell the user anything and everything, if instructed. As of writing, Wadsworth can handle integer, boolean and character data types.

**What about syntax?** 
    A later portion of this report will delve deeper into precisely how Wadsworth is given instructions, but here is a little teaser to get you excited. As explained earlier, Wadsworth is programmed in such a way that the code looks like proper English – and with proper English, we mean proper English: where other, more abstract programming languages tend to give more leeway in how they are written down, Wadsworth has been raised as a proper English butler, and therefore always expects his instructions to come in perfectly spelled and capitalised, with correct punctuation. This means that on a first look, Wadsworth's grammar might look very strict and hard-coded, but this has been a deliberate choice.

**Why not call it Jeeves or Alfred?** 
    Wadsworth was the only volunteer for this project, other butlers were too conservative in their ways and wished to stay with their employers, mowing their lawns and baking them cookies until their retirement.


- - -

####Using Wadsworth
You can compile the Wadsworth compiler yourself or use the jar file included.

(Not required if using .jar file)
Compiling the compiler yourself (jar files are included in the jarfiles folder):
1.  Add the antlr-4.5-complete.jar as library to your project 
2.  Add junit-4.7 as library to your project 
3.  Compile Main.java in src/Wadsworth
	
The program itself request the required paths, but it is also possible to pass command line arguments to Wadsworth:
1.  < file or folder path > [< outputpath > (if not specified, the input path is used)] [-s (silent (errors will still appear))]

Running a compiled class file:
1. open a terminal and move to the folder containing the class files
2. run: java < Classname >

- - -