You can compile the compiler yourself or use the jar file included.

(Not required if using .jar file)
Compiling the compiler yourself (jar files are included in the jarfiles folder) :
	- Add the antlr-4.5-complete.jar as library to your project 
	- Add junit-4.7 as library to your project 
	- Compile Main.java in src/Wadsworth
	
The program itself request the required paths, but it is also possible to pass an command line argument:
    - <file or folder path> [<outputpath> (if not specified, the input path is used)] [-s (silent (errors will still appear))]

Running class files:
    - open a terminal and move to the folder containing the class files
    - run: java <Classname>