package Wadsworth.Compiler;

class Program {
    private StringBuilder main = new StringBuilder();
    private String className;
    private int limitLocals;
    private int limitStack;


    public Program(String className) {
        this.className = className;
    }

    /**
     * Adds a new line to the main class of the program
     * @param line the line to add
     */
    public void addlineToMain(String line) {
        main.append(line + "\n");
    }

    /**
     * Sets the variabele with the amount of local variabeles
     * @param amount the local limit amount
     */
    public void setLocalLimit(int amount){
        this.limitLocals = amount;
    }

    /**
     * Sets the variabele with the stack size limit
     * @param amount the stack amount
     */
    public void setStackLimit(int amount){
        this.limitStack = amount;
    }

    /**
     * Returns the completed program at the point at which this method is called.
     * For completion's sake, the final line is appended in a separate String so that more lines can be appended to the program.
     * @return The completed program at this point
     */
    public String getProgram(){
        String result = getProgramStart();
        result += main.toString();
        result += "return\n";
        result += ".end method";
        return result;
    }

    /**
     * Returns the base of the program including the start of the main function
     * @return the string containing the start of the program until the main class
     */
    private String getProgramStart(){
        StringBuilder programBase = new StringBuilder();

        programBase.append(".class public " + className + "\n");
        programBase.append(".super java/lang/Object\n");

        programBase.append(".method public <init>()V\n");
        programBase.append("aload_0\n");
        programBase.append("invokenonvirtual java/lang/Object/<init>()V\n");
        programBase.append("return\n");
        programBase.append(".end method\n");
        programBase.append(".method public static main([Ljava/lang/String;)V\n");

        programBase.append(".limit locals " + limitLocals + "\n");
        programBase.append(".limit stack " + limitStack + "\n");
        return programBase.toString();
    }
}

