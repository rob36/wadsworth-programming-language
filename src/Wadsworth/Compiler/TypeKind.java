package Wadsworth.Compiler;

/** Pascal data type kind. */
public enum TypeKind {
	/** Integer base type. */
	INT,
	/** Boolean base type. */
	BOOL,
	/** Char base type. */
	CHAR,
	/** Array compound type. */
	ARRAY
}
