package Wadsworth.Compiler;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import Wadsworth.Grammar.WadsworthBaseListener;
import Wadsworth.Grammar.WadsworthParser;

import java.util.ArrayList;
import java.util.List;

public class WadsworthChecker extends WadsworthBaseListener {

    private Result result;
    private Scope scope;
    private List<String> constantList;

    private List<String> errors;

    public Result check(ParseTree tree, String filename) throws ParseException {
        this.result = new Result();
        this.scope = new Scope();
        this.errors = new ArrayList<>();
        this.constantList = new ArrayList<>();
        new ParseTreeWalker().walk(this, tree);
        if (!this.errors.isEmpty()) {
            System.out.println("Checked file " + filename);
            throw new ParseException(this.errors);
        }
        return this.result;
    }

    private void checkType(ParserRuleContext node, Type expected) {
        Type actual = getType(node);
        if (actual == null) {
            throw new IllegalArgumentException("Missing inferred type of "
                    + node.getText());
        }
        if (!actual.equals(expected)) {
            addError(node, "Expected type '%s' but found '%s'", expected,
                    actual);
        }
    }

    /* Convenience method to add a type to the result. */
    private void setType(ParseTree node, Type type) {
        this.result.setType(node, type);
    }

    /* Returns the type of a given expression or type node. */
    private Type getType(ParseTree node) {
        return this.result.getType(node);
    }

    /**
     * Records an error at a given parse tree node.
     *
     * @param node    the parse tree node at which the error occurred
     * @param message the error message
     * @param args    arguments for the message, see {@link String#format}
     */
    private void addError(ParserRuleContext node, String message,
                          Object... args) {
        addError(node.getStart(), message, args);
    }

    /**
     * Records an error at a given token.
     *
     * @param token   the token at which the error occurred
     * @param message the error message
     * @param args    arguments for the message, see {@link String#format}
     */
    private void addError(Token token, String message, Object... args) {
        int line = token.getLine();
        int column = token.getCharPositionInLine();
        message = String.format(message, args);
        message = String.format("Line %d:%d - %s", line, column, message);
        this.errors.add(message);
    }

    @Override
    public void exitNumExpr(WadsworthParser.NumExprContext ctx) {
        setType(ctx, Type.INT);
    }

    @Override
    public void exitFalseExpr(WadsworthParser.FalseExprContext ctx) {
        setType(ctx, Type.BOOL);
    }

    @Override
    public void exitTrueExpr(WadsworthParser.TrueExprContext ctx) {
        setType(ctx, Type.BOOL);
    }

    @Override
    public void exitCharExpr(WadsworthParser.CharExprContext ctx) {
        setType(ctx, Type.CHAR);
    }

    @Override
    public void exitVarDecl(WadsworthParser.VarDeclContext ctx) {
        Type ctxType = getType(ctx.type());
        if (!this.scope.put(ctx.ID().toString(), ctxType)) {
            addError(ctx, "Variable " + ctx.ID().getText() + " already declared");
        }
        if (getType(ctx.expr()) != ctxType) {
            addError(ctx, "Unable to assign " + ctx.expr().getText() + " to " + ctx.ID().toString() + ", incompatible types");
        }
    }

    @Override
    public void exitConstDecl(WadsworthParser.ConstDeclContext ctx) {
        Type ctxType = getType(ctx.type());
        this.constantList.add(ctx.ID().getText());
        if (!this.scope.put(ctx.ID().toString(), ctxType)) {
            addError(ctx, "Variable " + ctx.ID().getText() + " already declared");
        }
        if (getType(ctx.expr()) != ctxType) {
            addError(ctx, "Unable to assign " + ctx.expr().getText() + " to " + ctx.ID().toString() + ", incompatible types");
        }
    }

    @Override
    public void exitAssStat(WadsworthParser.AssStatContext ctx) {
        if(constantList.contains(ctx.target().getText())){
            addError(ctx, "Unable to assign " + ctx.expr().getText() + " to " + ctx.target().getText() + ", " + ctx.target().getText() + " is constant");
        }
        if (getType(ctx.expr()) != getType(ctx.target())) {
            addError(ctx, "Unable to assign " + ctx.expr().getText() + " to " + ctx.target().getText() + ", incompatible types");
        }
    }

    @Override
    public void exitIfStat(WadsworthParser.IfStatContext ctx) {
        checkType(ctx.expr(), Type.BOOL);
    }

    @Override
    public void exitWhileStat(WadsworthParser.WhileStatContext ctx) {
        checkType(ctx.expr(), Type.BOOL);
    }

    @Override
    public void exitIdTarget(WadsworthParser.IdTargetContext ctx) {
        String id = ctx.ID().getText();
        Type type = this.scope.type(id);
        if (type == null) {
            addError(ctx, "Variable " + id + " not declared.");
        } else {
            setType(ctx, type);
        }
    }

    @Override
    public void exitMultExpr(WadsworthParser.MultExprContext ctx) {
        checkType(ctx.expr(0), Type.INT);
        checkType(ctx.expr(1), Type.INT);
        setType(ctx, Type.INT);
    }

    @Override
    public void exitMinusExpr(WadsworthParser.MinusExprContext ctx) {
        checkType(ctx.expr(0), Type.INT);
        checkType(ctx.expr(1), Type.INT);
        setType(ctx, Type.INT);
    }

    @Override
    public void exitPrfExpr(WadsworthParser.PrfExprContext ctx) {

        if (ctx.prfOp().MINUS() != null) {
            checkType(ctx.expr(), Type.INT);
            setType(ctx, Type.INT);
        } else {
            checkType(ctx.expr(), Type.BOOL);
            setType(ctx, Type.BOOL);
        }
    }

    @Override
    public void exitBoolExpr(WadsworthParser.BoolExprContext ctx) {
        checkType(ctx.expr(0), Type.BOOL);
        checkType(ctx.expr(1), Type.BOOL);
        setType(ctx, Type.BOOL);
    }

    @Override
    public void exitPlusExpr(WadsworthParser.PlusExprContext ctx) {
        checkType(ctx.expr(0), Type.INT);
        checkType(ctx.expr(1), Type.INT);
        setType(ctx, Type.INT);
    }

    @Override
    public void exitCompExpr(WadsworthParser.CompExprContext ctx) {
        Type type1 = getType(ctx.expr(0));
        Type type2 = getType(ctx.expr(1));

        if (type1 == type2) {
            if (type1 != Type.INT) {
                if (ctx.compOp().EQ() == null && ctx.compOp().NE() == null) {
                    addError(ctx, "Comparative operator not applicable for these types");
                }
            }
        }
        setType(ctx, Type.BOOL);
    }

    @Override
    public void exitDivExpr(WadsworthParser.DivExprContext ctx) {
        checkType(ctx.expr(0), Type.INT);
        checkType(ctx.expr(1), Type.INT);
        setType(ctx, Type.INT);
    }

    @Override
    public void exitIdExpr(WadsworthParser.IdExprContext ctx) {
        String id = ctx.ID().getText();
        Type type = this.scope.type(id);
        if (type == null) {
            addError(ctx, "Variable '%s' not declared", id);
        } else {
            setType(ctx, type);
        }
    }

    @Override public void exitModExpr(WadsworthParser.ModExprContext ctx) {
        checkType(ctx.expr(0), Type.INT);
        checkType(ctx.expr(1), Type.INT);
        setType(ctx, Type.INT);
    }

    @Override
    public void exitIntType(WadsworthParser.IntTypeContext ctx) {
        result.setType(ctx, Type.INT);
    }

    @Override
    public void exitBoolType(WadsworthParser.BoolTypeContext ctx) {
        result.setType(ctx, Type.BOOL);
    }

    @Override
    public void exitCharType(WadsworthParser.CharTypeContext ctx) {
        result.setType(ctx, Type.CHAR);
    }
}
