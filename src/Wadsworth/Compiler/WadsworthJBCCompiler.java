package Wadsworth.Compiler;

import Wadsworth.Grammar.WadsworthBaseVisitor;
import Wadsworth.Grammar.WadsworthLexer;
import Wadsworth.Grammar.WadsworthParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileReader;
import java.io.IOException;
import java.lang.Override;
import java.util.HashMap;
import java.util.Map;

public class WadsworthJBCCompiler extends WadsworthBaseVisitor<String> {

    /*
     * The eventual program that will be ran through Jasmin to create the final .class file.
     */
    private Program program;

    /*
     * The total amount of registers currently in use.
     * The initial value of -1 is used to prevent VerifyErrors.
     */
    private int maxReg = -1;

    /*
     * This map maps the variable names to their respective register number.
     */
    private Map<String, Integer> regMap = new HashMap<>();

    /*
     * The result of the type checking.
     */
    private Result checkResult;

    /*
     * Stack counters, keeping track of the stack size. Used to set the required stack size in the class file.
     */
    private int stackSize;
    private int maxStackSize;

    /*
     * The number of if loops in the program.
     */
    private int numIf;

    /*
     * The number of while loops in the program.
     */
    private int numWhile;

    /*
     * The number of comparative operations in the program.
     */
    private int numComp;

    /**
     * Compiles a given .Wadsworth file to Java ByteCode instructions, ready to be made into a .class file by Jasmin.
     * @param filepath the filepath containing the .wadsworth file
     */
    public String compile(String filepath) {
        String filename = filepath.replace(".Wadsworth", "");
        filename = filename.substring(filename.lastIndexOf('/') + 1);

        program = new Program(filename);
        ErrorListener listener = new ErrorListener();
        try {
            ANTLRInputStream stream = new ANTLRInputStream(new FileReader(filepath));
            WadsworthLexer lexer = new WadsworthLexer(stream);
            lexer.removeErrorListeners();
            lexer.addErrorListener(listener);
            TokenStream tokens = new CommonTokenStream(lexer);
            WadsworthParser parser = new WadsworthParser(tokens);
            parser.removeErrorListeners();
            parser.addErrorListener(listener);
            ParseTree tree = parser.program();

            //run the type checker
            WadsworthChecker checker = new WadsworthChecker();
            this.checkResult = checker.check(tree, filename);

            tree.accept(this);
            //set the program limits
            this.program.setLocalLimit(maxReg + 2);
            this.program.setStackLimit(maxStackSize);
            listener.throwException();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            System.out.println("Error checking file: " + filename);
            e.printStackTrace();
        }
        return program.getProgram();
    }

    /*
     * Help method to either return the register assigned to the given variable or,
     * if the variable does not yet have a register assigned to it, to assign it a register.
     */
    private int getOrSetReg(String var) {
        if (regMap.containsKey(var)) {
            return regMap.get(var);
        } else {
            maxReg += 1;
            regMap.put(var, maxReg);
            return maxReg;
        }
    }


    /**
     * Increases the current stack and the max stack if the current stack exceeds the max stack
     */
    private void stackUp() {
        stackSize++;
        if (stackSize > maxStackSize) {
            maxStackSize = stackSize;
        }
    }

    /**
     * Decreases the current stack size
     */
    private void stackDown() {
        stackSize--;
    }

    /**
     * Overridden methods
     */
    @Override
    public String visitVarDecl(WadsworthParser.VarDeclContext ctx) {
        visit(ctx.ID());
        String result = ctx.ID().getText();
        int reg = getOrSetReg(result);

        visit(ctx.expr());
        program.addlineToMain("istore " + reg);

        stackDown();

        return result;
    }

    @Override
    public String visitConstDecl(WadsworthParser.ConstDeclContext ctx) {
        visit(ctx.ID());
        String result = ctx.ID().getText();
        int reg = getOrSetReg(result);

        visit(ctx.expr());
        program.addlineToMain("istore " + reg);

        stackDown();

        return result;
    }

    @Override
    public String visitAssStat(WadsworthParser.AssStatContext ctx) {
        visit(ctx.target());
        String result = ctx.target().getText();
        int reg = getOrSetReg(result);

        visit(ctx.expr());
        program.addlineToMain("istore " + reg);

        stackDown();

        return result;
    }

    @Override
    public String visitIfStat(WadsworthParser.IfStatContext ctx) {
        String endIfString = "EndIf_" + numIf;

        visit(ctx.expr());
        if(ctx.ELSE() != null) {
            String elseString = "Else_" + numIf;

            program.addlineToMain("ifeq " + elseString);
            visit(ctx.block(0));
            program.addlineToMain("goto " + endIfString);

            program.addlineToMain(elseString + ":");
            visit(ctx.block(1));
            program.addlineToMain("goto " + endIfString);
        } else {
            program.addlineToMain("ifeq " + endIfString);
            visit(ctx.block(0));
            program.addlineToMain("goto " + endIfString);
        }
        program.addlineToMain(endIfString + ":");

        numIf++;
        return endIfString;
    }

    @Override
    public String visitWhileStat(WadsworthParser.WhileStatContext ctx) {
        String whileString = "While_" + numWhile;
        program.addlineToMain(whileString + ":");
        numWhile++;

        visit(ctx.block());
        visit(ctx.expr());
        program.addlineToMain("ifne " + whileString);
        return whileString;
    }

    @Override
    public String visitInStat(WadsworthParser.InStatContext ctx) {
        program.addlineToMain("new java/util/Scanner");
        program.addlineToMain("dup");
        program.addlineToMain("getstatic java/lang/System/in Ljava/io/InputStream;");
        program.addlineToMain("invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V");
        if (ctx.INFLAVOUR() != null) {
            program.addlineToMain("getstatic java/lang/System/out Ljava/io/PrintStream;");
            visit(ctx.STR());
            String printString = ctx.STR().toString();
            program.addlineToMain("ldc " + printString);
            program.addlineToMain("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        }

        Type type = checkResult.getType(ctx.type());
        String target = handleInput(ctx, type);

        stackUp();
        stackUp();
        stackUp();
        return target;
    }

    /**
     * Help method for handling interactive input.
     */
    private String handleInput(WadsworthParser.InStatContext ctx, Type type) {
        String target = visit(ctx.target());
        int targetReg = getOrSetReg(target);

        if (type != Type.CHAR) {
            program.addlineToMain("invokevirtual java/util/Scanner.nextInt()I");
            program.addlineToMain("istore " + targetReg);
        } else {
            program.addlineToMain("invokevirtual java/util/Scanner.next()Ljava/lang/String;");
            program.addlineToMain("iconst_0");
            program.addlineToMain("invokevirtual java/lang/String.charAt(I)C");
            program.addlineToMain("istore " + targetReg);
        }

        return target;
    }

    @Override
    public String visitPrintStat(WadsworthParser.PrintStatContext ctx) {
        program.addlineToMain("getstatic java/lang/System/out Ljava/io/PrintStream;");

        if (!(ctx.expr() == null)) {
            visit(ctx.expr());
            Type type = checkResult.getType(ctx.expr());
            if(type == Type.CHAR){
                program.addlineToMain("invokestatic java/lang/Character/toChars(I)[C");
                program.addlineToMain("invokevirtual java/io/PrintStream/println([C)V");
            } else {
                program.addlineToMain("invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
                program.addlineToMain("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
            }
        } else {
            visit(ctx.STR());
            String printString = ctx.STR().toString();
            program.addlineToMain("ldc " + printString);
            program.addlineToMain("invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
        }

        stackUp();
        stackUp();
        return null;
    }

    @Override
    public String visitIdTarget(WadsworthParser.IdTargetContext ctx) {
        return ctx.getText();
    }

    @Override
    public String visitMultExpr(WadsworthParser.MultExprContext ctx) {
        String result = visit(ctx.expr().get(0));
        visit(ctx.expr().get(1));

        program.addlineToMain("imul");
        stackDown();
        return result;
    }

    @Override
    public String visitMinusExpr(WadsworthParser.MinusExprContext ctx) {
        String result = visit(ctx.expr().get(0));
        visit(ctx.expr().get(1));

        program.addlineToMain("isub");
        stackDown();
        return result;
    }

    @Override
    public String visitPrfExpr(WadsworthParser.PrfExprContext ctx) {
        String result = visit(ctx.expr());
        if (ctx.prfOp().MINUS() != null) {
            program.addlineToMain("ineg");
        } else {
            program.addlineToMain("iconst_0");
            stackUp();
            program.addlineToMain("iand");
            stackDown();
        }
        return result;
    }

    @Override
    public String visitNumExpr(WadsworthParser.NumExprContext ctx) {
        String result = ctx.NUM().toString();
        program.addlineToMain("bipush " + result);
        stackUp();
        return result;
    }

    @Override
    public String visitFalseExpr(WadsworthParser.FalseExprContext ctx) {
        String result = ctx.FALSE().toString();
        program.addlineToMain("iconst_0");
        stackUp();
        return result;
    }

    @Override
    public String visitTrueExpr(WadsworthParser.TrueExprContext ctx) {
        String result = ctx.TRUE().toString();
        program.addlineToMain("iconst_1");
        stackUp();
        return result;
    }

    @Override
    public String visitCharExpr(WadsworthParser.CharExprContext ctx) {
        String result = ctx.CHAR().toString();
        char res = result.charAt(1);
        program.addlineToMain("bipush " + (int) res);
        stackUp();
        return result;
    }

    @Override
    public String visitIdExpr(WadsworthParser.IdExprContext ctx) {
        String result = ctx.getText();
        int reg = getOrSetReg(result);
        program.addlineToMain("iload " + reg);

        stackUp();
        return result;
    }

    @Override public String visitModExpr(WadsworthParser.ModExprContext ctx) {
        String result = visit(ctx.expr().get(0));
        visit(ctx.expr().get(1));

        program.addlineToMain("irem");
        stackDown();
        return result;
    }

    @Override
    public String visitBoolExpr(WadsworthParser.BoolExprContext ctx) {
        String result = ctx.boolOp().getText();
        visit(ctx.expr(0));
        visit(ctx.expr(1));

        if (result.equals("&&")) {
            program.addlineToMain("iand");
        } else {
            program.addlineToMain("ior");
        }

        stackDown();
        return result;
    }

    @Override
    public String visitPlusExpr(WadsworthParser.PlusExprContext ctx) {
        String result = visit(ctx.expr().get(0));
        visit(ctx.expr().get(1));

        program.addlineToMain("iadd");
        stackDown();
        return result;
    }

    @Override
    public String visitCompExpr(WadsworthParser.CompExprContext ctx) {
        String result = ctx.compOp().getText();
        visit(ctx.expr(0));
        visit(ctx.expr(1));

        String falseString = "False_" + numComp;
        String endString = "End_"     + numComp;
        numComp++;

        switch(result) {
            case " is lesser than or equal to ":
                program.addlineToMain("if_icmpgt " + falseString);
                break;
            case " is lesser than ":
                program.addlineToMain("if_icmpge " + falseString);
                break;
            case " is greater than or equal to ":
                program.addlineToMain("if_icmplt " + falseString);
                break;
            case " is greater than ":
                program.addlineToMain("if_icmple " + falseString);
                break;
            case " is equal to ":
                program.addlineToMain("if_icmpne " + falseString);
                break;
            case " is not equal to ":
                program.addlineToMain("if_icmpeq " + falseString);
                break;
        }
        stackDown();
        stackDown();

        program.addlineToMain("iconst_1");
        program.addlineToMain("goto " + endString);
        program.addlineToMain(falseString + ":");
        program.addlineToMain("iconst_0");
        program.addlineToMain("goto " + endString);
        program.addlineToMain(endString + ":");
        stackUp();
        return result;
    }

    @Override
    public String visitDivExpr(WadsworthParser.DivExprContext ctx) {
        String result = visit(ctx.expr().get(0));
        visit(ctx.expr().get(1));

        program.addlineToMain("idiv");
        stackDown();
        return result;
    }
}
