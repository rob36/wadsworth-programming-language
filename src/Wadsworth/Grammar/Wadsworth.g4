grammar Wadsworth;

@header{package Wadsworth.Grammar;}

/** Call Wadsworth into the room to give him his tasks for the day */
program: COMMAND BEGINPROGRAM ENDCOMMAND task* ENDPROGRAM;

/** Give good ol' Wadsworth a task. */
task: COMMAND (stat | decl) ENDCOMMAND;

/** Have Wadsworth declare some variables and/or constants for you. */
decl: VARDECL type NAMED ID VAL expr        #varDecl
    | CONSTDECL type NAMED ID VAL expr      #constDecl
    ;

/** Statements that will be useful in instructing Wadsworth. */
stat: ASS expr TO target                    #assStat
    | IF expr THEN block (ELSE block)?      #ifStat
    | WHILE expr DO block                   #whileStat
    | IN type NAMED target (INFLAVOUR STR)? #inStat
    | PRINT (STR | expr)                    #printStat
    ;

/** Lists of tasks Wadsworth has to execute seperately */
block: BEGINBLOCK task+ ENDBLOCK ;

/** Target of an assignment. */
target
    : ID                #idTarget
    ;

/** Expressions for Wadsworth to use. */
expr: prfOp expr            #prfExpr
    | LPAR expr RPAR        #parExpr
    | expr STAR expr        #multExpr
    | expr SLASH expr       #divExpr
    | expr MOD expr         #modExpr
    | expr PLUS expr        #plusExpr
    | expr MINUS expr       #minusExpr
    | expr compOp expr      #compExpr
    | expr boolOp expr      #boolExpr
    | TRUE                  #trueExpr
    | FALSE                 #falseExpr
    | ID                    #idExpr
    | NUM                   #numExpr
    | CHAR                  #charExpr
    ;

/** Wadsworth will compare with these. */
compOp: LE | LT | GE | GT | EQ | NE;

/** Wadsworth will prefix all sorts of things with these. */
prfOp: MINUS | NOT;

/** Boolean operators for Wadsworth to use. */
boolOp: AND | OR;

/** Data types Wadsworth recognises. */
type: INTEGER       #intType
    | BOOLEAN       #boolType
    | CHARACTER     #charType
    ;

INTEGER:        ' number';
BOOLEAN:        ' boolean';
CHARACTER:      ' character';

/** Keywords Wadsworth uses in his tasks. */
AND:            ' and ';
ASS:            ' assign ';
BEGINBLOCK:     ' carry out these tasks:';
BEGINPROGRAM:   ' come in for a moment';
COMMAND:        'Wadsworth, would you';
CONSTDECL:      ' create an unchangeable ';
DOT:            '.';
DQUOTE:         '"';
ENDBLOCK:       'Take care of it';
ENDCOMMAND:     ', please?';
ENDPROGRAM:     'You are dismissed.';
EQ:             ' is equal to ';
GE:             ' is greater than or equal to ';
GT:             ' is greater than ';
IF:             ' verify that ';
IN:             ' humbly request a';
LE:             ' is lesser than or equal to ';
LPAR:           '(';
LT:             ' is lesser than ';
MINUS:          ' minus ';
MOD:            ' modulo ';
NE:             ' is not equal to ';
NOT:            'not ';
OR:             ' or ';
PLUS:           ' plus ';
PRINT:          ' tell our humble guests ';
RPAR:           ')';
SQUOTE:         '\'';
SLASH:          ' divided by ';
STAR:           ' multiplied by ';
VARDECL:        ' create a';
WHILE:          ', as long as ';

/** Keywords that are used to give Wadsworths tasks that extra bit of flavour. */
DO:             ' holds, be so kind as to';
ELSE:           ' Otherwise,';
INFLAVOUR:      '? Ask them ';
NAMED:          ' named ';
THEN:           '? If so,';
TO:             ' to ';
VAL:            ' with value ';

/** Content Wadsworth can use. */
TRUE: 'Positive';
FALSE: 'Negative';
ID: LETTER (LETTER | DIGIT)*;
NUM: DIGIT (DIGIT)*;
STR: DQUOTE .*? DQUOTE;
CHAR: SQUOTE LETTER SQUOTE;

fragment LETTER: [a-zA-Z];
fragment DIGIT: [0-9];

/** Wadsworth will ignore these. */
COMMENT: SLASH STAR .*? STAR SLASH  -> skip;
WS: [ \t\r\n]+ -> skip;