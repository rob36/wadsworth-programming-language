// Generated from /home/clementr/Projects/wadsworth-programming-language/src/Wadsworth/Grammar/Wadsworth.g4 by ANTLR 4.5
package Wadsworth.Grammar;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link WadsworthParser}.
 */
public interface WadsworthListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(WadsworthParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(WadsworthParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#task}.
	 * @param ctx the parse tree
	 */
	void enterTask(WadsworthParser.TaskContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#task}.
	 * @param ctx the parse tree
	 */
	void exitTask(WadsworthParser.TaskContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varDecl}
	 * labeled alternative in {@link WadsworthParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterVarDecl(WadsworthParser.VarDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varDecl}
	 * labeled alternative in {@link WadsworthParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitVarDecl(WadsworthParser.VarDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constDecl}
	 * labeled alternative in {@link WadsworthParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterConstDecl(WadsworthParser.ConstDeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constDecl}
	 * labeled alternative in {@link WadsworthParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitConstDecl(WadsworthParser.ConstDeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterAssStat(WadsworthParser.AssStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitAssStat(WadsworthParser.AssStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(WadsworthParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(WadsworthParser.IfStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterWhileStat(WadsworthParser.WhileStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitWhileStat(WadsworthParser.WhileStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterInStat(WadsworthParser.InStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitInStat(WadsworthParser.InStatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterPrintStat(WadsworthParser.PrintStatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printStat}
	 * labeled alternative in {@link WadsworthParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitPrintStat(WadsworthParser.PrintStatContext ctx);
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(WadsworthParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(WadsworthParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idTarget}
	 * labeled alternative in {@link WadsworthParser#target}.
	 * @param ctx the parse tree
	 */
	void enterIdTarget(WadsworthParser.IdTargetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idTarget}
	 * labeled alternative in {@link WadsworthParser#target}.
	 * @param ctx the parse tree
	 */
	void exitIdTarget(WadsworthParser.IdTargetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultExpr(WadsworthParser.MultExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultExpr(WadsworthParser.MultExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prfExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPrfExpr(WadsworthParser.PrfExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prfExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPrfExpr(WadsworthParser.PrfExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(WadsworthParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(WadsworthParser.IdExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterModExpr(WadsworthParser.ModExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitModExpr(WadsworthParser.ModExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(WadsworthParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(WadsworthParser.BoolExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plusExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPlusExpr(WadsworthParser.PlusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plusExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPlusExpr(WadsworthParser.PlusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCompExpr(WadsworthParser.CompExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCompExpr(WadsworthParser.CompExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterParExpr(WadsworthParser.ParExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitParExpr(WadsworthParser.ParExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minusExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMinusExpr(WadsworthParser.MinusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minusExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMinusExpr(WadsworthParser.MinusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNumExpr(WadsworthParser.NumExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNumExpr(WadsworthParser.NumExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code trueExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterTrueExpr(WadsworthParser.TrueExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code trueExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitTrueExpr(WadsworthParser.TrueExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code falseExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFalseExpr(WadsworthParser.FalseExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code falseExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFalseExpr(WadsworthParser.FalseExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCharExpr(WadsworthParser.CharExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCharExpr(WadsworthParser.CharExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code divExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterDivExpr(WadsworthParser.DivExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code divExpr}
	 * labeled alternative in {@link WadsworthParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitDivExpr(WadsworthParser.DivExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#compOp}.
	 * @param ctx the parse tree
	 */
	void enterCompOp(WadsworthParser.CompOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#compOp}.
	 * @param ctx the parse tree
	 */
	void exitCompOp(WadsworthParser.CompOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void enterPrfOp(WadsworthParser.PrfOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void exitPrfOp(WadsworthParser.PrfOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link WadsworthParser#boolOp}.
	 * @param ctx the parse tree
	 */
	void enterBoolOp(WadsworthParser.BoolOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link WadsworthParser#boolOp}.
	 * @param ctx the parse tree
	 */
	void exitBoolOp(WadsworthParser.BoolOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void enterIntType(WadsworthParser.IntTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void exitIntType(WadsworthParser.IntTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void enterBoolType(WadsworthParser.BoolTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void exitBoolType(WadsworthParser.BoolTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void enterCharType(WadsworthParser.CharTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charType}
	 * labeled alternative in {@link WadsworthParser#type}.
	 * @param ctx the parse tree
	 */
	void exitCharType(WadsworthParser.CharTypeContext ctx);
}