package Wadsworth;

import Wadsworth.Compiler.WadsworthJBCCompiler;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    private static String outputPath;
    private static Boolean debugging;
    private static File jasminJar;
    private static String defaultJasminPath = "jarfiles/jarmin/jasmin.jar";

    private final static String SILENTFLAG = "-s";

    public static void main(String[] args) {
        String inputPath;
        Scanner input = new Scanner(System.in);
        if (args.length == 0) {
            System.out.println("Wadsworth can handle commandline arguments: <file or folder path> [<outputpath>] (if not specified, the input path is used)] [-s (silent (errors will still appear))] ");
            System.out.println("Because wadsworth is such a good listener, you could also just tell him: ");

            System.out.println("Wadsworth needs an input path (file or folder), Wadsworth is awaiting your response...");
            inputPath = input.next();
            System.out.println("Wadsworth could use an output path, but is not required. If you tell him 'no', he will use the input path as output path. Wadsworth is awaiting your response...");
            outputPath = input.next();
            if (outputPath.equals("no")) {
                outputPath = inputPath;
            }
            System.out.println("Wadsworth asks you if you would like to receive updates about what he's doing. Wadsworth is awaiting your response... (yes)");
            debugging = input.next().equals("yes");

        } else {
            //first argument is the inputpath
            inputPath = args[0];
            int argslength = args.length;
            if (argslength == 2) {
                if (args[1].equals(SILENTFLAG)) {
                    debugging = false;
                } else {
                    debugging = true;
                    outputPath = args[1];
                }
            } else if (argslength == 3) {
                if (args[2].equals(SILENTFLAG)) {
                    debugging = false;
                    outputPath = args[1];
                } else {
                    outputPath = args[2];
                    debugging = args[2].equals(SILENTFLAG);
                }
            }
        }
        if (!outputPath.substring(outputPath.length() - 1).equals("/")) {
            outputPath += '/';
        }
        File f = new File(inputPath);

        while (jasminJar == null) {
            File jasmin = new File(defaultJasminPath);
            if (!jasmin.isFile()) {
                System.out.println("Wadsworth could not find the jasmin jar file, please provide the path to the jasmin jar file: ");
                defaultJasminPath = input.next();
            } else {
                jasminJar = jasmin;
            }
        }
        if (f.isDirectory()) {
            print("Wadsworth will now start compiling all files in: " + inputPath);
            compileFolder(f);
        } else {
            compileFile(f);
        }
    }

    //compiles all ".Wadsworth" files inside a folder
    private static void compileFolder(File folder) {
        for (File f : folder.listFiles()) {
            String path = f.getPath();
            int dot = path.lastIndexOf('.');
            if (dot > 0) {
                String extension = path.substring(dot);
                if (extension.equals(".Wadsworth")) {
                    compileFile(f);
                }
            }
        }
    }

    //compile a single file
    private static void compileFile(File f) {
        print("Start compiling " + f.getName());
        try {
            WadsworthJBCCompiler c = new WadsworthJBCCompiler();
            //compile the program to jasmin
            String compiled = c.compile(f.getPath());

            //create new filenames/paths
            String filename = f.getName().replace(".Wadsworth", "");
            String jasminFileLocation = outputPath + filename + ".j";

            //create output location and write the jasmin code to a file
            File file = new File(jasminFileLocation);
            file.getParentFile().mkdirs();

            //write the jasmin file
            PrintWriter out = new PrintWriter(file);

            out.write(compiled);
            out.close();

            File output = new File(outputPath);

            //call jasmin compiler
            ProcessBuilder pb = new ProcessBuilder("java", "-jar", jasminJar.getAbsolutePath(), "-d", output.getAbsolutePath(), file.getAbsolutePath());
            if (debugging) {
                pb.inheritIO();
            }
            Process p = pb.start();
            p.waitFor();
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
        print("done compiling " + f.getName());
    }

    private static void print(String message) {
        if (debugging) {
            System.out.println("----- " + message + " -----");
        }
    }

}
