package Wadsworth.Test;

import org.junit.*;
import Wadsworth.Main;

import java.io.*;


public class WadsworthTest {
    private static String outputPath = "src/Wadsworth/Test/out/";
    private static String testFolder = "src/Wadsworth/Test/TestPrograms";


    /*  List of tests executed in this program that do not require input
        Format: {<classname>, <expected output>}
     */
    private static String[][] toTestWithoutInput = {
            {"helloWorld", "Hello world!"},
            {"testAssign", "2"},
            {"testDivAndMin", "2"},
            {"testExpr", "9"},
            {"testIf", "1"},
            {"testModulo", "17"},
            {"testNot", "0"},
            {"testWhile", "3210"},
            {"whileIfNested", "10"}
    };

    /*  List of tests executed in this program that do not require input
        Format: {<classname>, <input string>, <expected output>}
    */
    private static String[][] toTestWithInput = {
            {"charInputOutputTest", "a", "Enter a char:a"},
            {"numberInputOutputTest", "543", "Enter a number:543"},
            {"booleanInputOutputTest", "1", "Enter a boolean:1"}
            // {"manualModulus", "12\n5", "iets"}
    };

    /**
     * compiles all files in the TestPrograms folder
     */
    @Before
    public void compileAll() {
        String[] arg = {testFolder, outputPath, "-s"};
        System.out.println("----- Compiling all files for testing -----");
        Main.main(arg);
        System.out.println("----- Done compiling all files -----");
    }

    /**
     * tests all files specified in the toTestWithoutInput array
     */
    @Test
    public void testWithoutInput() {
        for (String[] current : toTestWithoutInput) {
            String name = current[0];
            System.out.println("----- Testing " + name + " -----");
            String result = runJavaClass(name, null);
            Assert.assertEquals("Test " + name + " should have returned " + current[1], current[1], result);
            System.out.println("----- End of test " + name + " -----");
        }
    }

    @Test
    public void testWithInput() {
        for (String[] current : toTestWithInput) {
            String name = current[0];
            System.out.println("----- Testing " + name + " -----");
            String result = runJavaClass(name, current[1]);
            Assert.assertEquals("Test " + name + " should have returned " + current[2], current[2], result);
            System.out.println("----- End of test " + name + " -----");
        }
    }


    /*
    Creates a String from a bufferedreader
     */
    private String readerToString(BufferedReader r) throws IOException {
        String line;
        StringBuilder builder = new StringBuilder();
        while ((line = r.readLine()) != null) {
            builder.append(line);
        }
        return builder.toString();
    }

    /*
    Runs the class with the classname, handles the input and returns the output
     */
    private String runJavaClass(String className, String input) {
        File outputfolder = new File(outputPath);
        File output = new File(outputfolder.getAbsolutePath());
        ProcessBuilder pb = new ProcessBuilder("java", className);
        pb.directory(output);
        String returnValue = null;
        try {
            Process p = pb.start();
            if (input != null) {
                OutputStream out = p.getOutputStream();
                BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(out));
                wr.write(input);
                wr.newLine();
                wr.flush();
                wr.close();
            }
            p.waitFor();
            InputStreamReader str = new InputStreamReader(p.getInputStream());
            BufferedReader r = new BufferedReader(str);
            returnValue = readerToString(r);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return returnValue;
    }
}
