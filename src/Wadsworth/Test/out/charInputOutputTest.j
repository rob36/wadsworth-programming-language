.class public charInputOutputTest
.super java/lang/Object
.method public <init>()V
aload_0
invokenonvirtual java/lang/Object/<init>()V
return
.end method
.method public static main([Ljava/lang/String;)V
.limit locals 2
.limit stack 6
bipush 97
istore 0
new java/util/Scanner
dup
getstatic java/lang/System/in Ljava/io/InputStream;
invokespecial java/util/Scanner/<init>(Ljava/io/InputStream;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Enter a char:"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
invokevirtual java/util/Scanner.next()Ljava/lang/String;
iconst_0
invokevirtual java/lang/String.charAt(I)C
istore 0
getstatic java/lang/System/out Ljava/io/PrintStream;
iload 0
invokestatic java/lang/Character/toChars(I)[C
invokevirtual java/io/PrintStream/println([C)V
return
.end method